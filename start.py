from twisted.internet.protocol import ClientFactory
from twisted.internet.protocol import Protocol
from twisted.internet import reactor
from twisted.internet.defer import DeferredQueue
from twisted.internet.task import LoopingCall
from battleship import *
import sys
import re

PORT = 9001
player_num = -1


class Player(Protocol):
  def __init__(self):
    self.g_space = GameSpace(player_num)
    self.create_regex_objects()
    self.send_board()
  def create_regex_objects(self):
    '''
      creates regex expressions used to match data sent from other player
    '''
    self.coordinates_regex = re.compile("^x:[0-9]+.[0-9]* y:[0-9]+.[0-9]+$")
    self.gameover = re.compile("Gameover")
    self.game_board_regex = re.compile("Gameboard:")
    self.attack_result_regex = re.compile("HIT|MISS")

  def dataReceived(self, data):
    if self.coordinates_regex.match(data):
      data_split = data.split(" ")
      try:
        x = float(data_split[0][2:])
        y = float(data_split[1][2:])
      except ValueError as e:
        print e
      result = self.g_space.opponent_attack(x, y)
    elif self.game_board_regex.match(data):
      board = data[len("Gameboard:"):].split(',') # get rid of beginning of string and split string to make the board
      board = [board[x:x+10] for x in xrange(0, len(board), 10)]
      for i in range(len(board)):
        for j in range(len(board[0])):
          try:
            new_tile = int(board[i][j])
            board[i][j] = new_tile
          except ValueError:
            pass
      # set opponent board so that we can start game
      self.g_space.add_opponents_board(board)

  def send_board(self):
    board_string = "Gameboard:" + ','.join([str(tile) for row in self.g_space.game_board for tile in row])
    queue.put(board_string)
  def connectionMade(self):
    print "connectionMade"
    queue.get().addCallback(self.cb)
    self.lc = LoopingCall(self.g_space.main)
    self.lc.start(1/60.0)
  def cb(self, data):
    self.transport.write(data)
    queue.get().addCallback(self.cb)

class PlayerFactory(ClientFactory):
  def __init__(self):
    pass
  def buildProtocol(self, addr):
    return Player()

def Usage():
  print '''Usage: python start.py [player_num]
  player_num can equal only 1 or 2'''
  sys.exit(1)

if __name__=='__main__':
  if len(sys.argv) != 2:
    Usage()
  try:
    player_num = int(sys.argv[1])
  except ValueError:
    Usage()
  if player_num == 1:
    reactor.listenTCP(PORT, PlayerFactory())
  elif player_num == 2:
    # game starts when player 2 connects
    reactor.connectTCP("localhost", PORT, PlayerFactory())
  else:
    Usage()
  reactor.run()
