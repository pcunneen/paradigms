#!/usr/bin/env python
import pygame, math, sys
import random
from pygame.locals import *
from twisted.internet.defer import DeferredQueue
from twisted.python import log
import time



#Colors which will be used by the game
BLACK   = (  0,   0,   0)
RED 	= ( 255,  0,   0)
WHITE   = (255, 255, 255)
GREEN   = (  0, 204,   0)
GRAY    = ( 60,  60,  60)
BLUE    = (  0,  50, 255)
YELLOW  = (255, 255,   0)
DARKGRAY =( 40,  40,  40)

# sends all communication between players
queue = DeferredQueue()


class GameSpace(object):

	def __init__(self, curr_player):
		self.gameover = 0 #set to 1 when game is over
		self.turn = 1
		self.lives = 0
		self.player_num = curr_player
		pygame.init()

		# initialize board dimensions
		self.size = self.width, self.height = 800, 600
		self.boardwidth = 10
		self.boardheight = 10
		self.displaywidth = 200
		self.tilesize = 40
		self.xmargin = int((self.width - (self.boardwidth*self.tilesize) - self.displaywidth-40)/2)
		self.ymargin = int((self.height - (self.boardheight*self.tilesize) - 40)/2)
		self.screen = pygame.display.set_mode(self.size)
		pygame.display.set_caption('BattleShip  Player:{}'.format(self.player_num))
		self.background = pygame.Surface(self.screen.get_size())
		self.background = self.background.convert()
		self.background.fill((0,0,0))
		self.screen.blit(self.background, (0,0)) 
		self.game_board = self.generate_default_tiles(0)
		self.ships = ['battleship', 'cruiser','destroyer','submarine'] #list of ships which are on the board
		self.game_board = self.add_ships() 
		self.opponent_board = []

		self.turn_surf = pygame.font.Font('freesansbold.ttf', 20).render("Player Turn: ", True, WHITE)
		self.screen.blit(self.turn_surf, (self.width-250, self.height-370))
		self.turn_id_surf = pygame.font.Font('freesansbold.ttf', 20).render("", True, WHITE)
		self.screen.blit(self.turn_id_surf, (self.width-250, self.height-340))

		self.opp_attack_surf = pygame.font.Font('freesansbold.ttf', 20).render("Opponent Attack Result: ", True, WHITE)
		self.screen.blit(self.opp_attack_surf, (self.width-250, self.height-250))
		self.opp_attack_id_surf = pygame.font.Font('freesansbold.ttf', 20).render("", True, WHITE)
		self.screen.blit(self.opp_attack_id_surf, (self.width-250, self.height-200))
		

		self.draw_board()
		pygame.display.flip()
	def add_opponents_board(self, board):
		# called in start.py
		self.opponent_board = board
	def is_turn(self):
		return self.turn % 2 == (self.player_num - 1)

	def did_lose(self):
		return self.lives == 0
	def draw_board(self):
		'''
		this function draws the ships as well as the grid on the screen
		'''
		for tx in xrange(self.boardwidth):
			for ty in xrange(self.boardheight):
				left = tx * self.tilesize + self.xmargin + 40
				top = ty * self.tilesize + self.ymargin + 40
				pygame.draw.rect(self.screen, BLUE, (left, top, self.tilesize, self.tilesize))

		for x in xrange(0, (self.boardwidth+1) * self.tilesize, self.tilesize):
			pygame.draw.line(self.screen, DARKGRAY, (x+self.xmargin+40, self.ymargin+40), (x+self.xmargin+40, self.height-self.ymargin))

		for y in xrange(0, (self.boardheight+1) * self.tilesize, self.tilesize):
			pygame.draw.line(self.screen, DARKGRAY, (self.xmargin+40, y+self.ymargin+40), (self.width-(self.displaywidth+80), y+self.ymargin+40))

	def generate_default_tiles(self,val):
		''' 
		This function creates a list of the board tiles as tuples.
		Format of the tupes is ('ship', hitStatus) with hitStatus = val
		'''
		default = [[val]*self.boardheight for i in xrange(self.boardwidth)]
		return default


	def add_ships(self):
		'''
		this function randomly places the ships on the board
		boundary checking is not done in thiie function
		returns a list of board tiles with ships placed on certain tiles
		'''
		new_board = self.game_board[:]
		ship_len = 0
		for s in self.ships:
			valid_pos = False
			while not valid_pos:
				xStart = random.randint(0,9)
				yStart = random.randint(0,9)
				setHoriz = random.randint(0,1) #orientation of ships

				if 'battleship' in s:
					ship_len = 4
				elif 'destroyer' in s:
					ship_len = 3
				elif 'cruiser' in s:
					ship_len = 2
				elif 'submarire' in s:
					ship_len = 2

				valid_pos , coords = self.set_ship(new_board, xStart, yStart, setHoriz, ship_len, s)

				if valid_pos:
					for c in coords:
						new_board[c[0]][c[1]] = s
					self.lives += ship_len
		return new_board

	def set_ship(self, board, x, y, setHoriz, length, ship):
		'''
		this function places a ship on the board given the starting position
		must check to make sure that the ship does not go out of bounds when placed
		as well as that the new ship does not intersect any existing ships
		'''
		coords = []

		if setHoriz:
			for i in xrange(length):
				if (i+x > 9) or (board[i+x][y] != 0) or self.hasAdj(board, i+x, y, ship):
					return (False, coords)
				else:
					coords.append((i+x,y))
		else:
			for i in xrange(length):
				if (i+y > 9 or board[x][i+i] != 0) or self.hasAdj(board, i, y+i, ship):
					return (False, coords)
				else:
					coords.append((x,y+i))
		return (True, coords) #success

	def hasAdj(self, board, x, y, ship):
		'''this function checks if there is a ship adjacent to the one trying to be placed
			returns true if there are adj ships and false otherwise
		'''
		for i in xrange(x-1,x+2):
			for j in xrange(y-1,y+2):
				if (i in range (10)) and (j in range (10)) and (board[i][j] not in (ship,0)):
					return True
		return False

	def coord_to_tile(self, x, y):
		'''
		this function will return the tile which corresponds to the coordinate the user clicks
		returns tuple (x-index, y-index)
		'''
		if x < self.xmargin + 40 or x > (9*self.tilesize + self.xmargin + 40 + self.tilesize):
			return False
		if y < self.ymargin + 40 or y > (9*self.tilesize + self.ymargin + 40 + self.tilesize):
			return None
		for tx in xrange(self.boardwidth):
			for ty in xrange(self.boardheight):
				left = tx * self.tilesize + self.xmargin + 40
				top = ty * self.tilesize + self.ymargin + 40
				tile_rect = pygame.Rect(left, top, self.tilesize, self.tilesize)
				if tile_rect.collidepoint(x,y):
					return (tx, ty)
		return (None, None)
	def end_game_screen(self, did_win):
		self.background.fill((0,0,0))
		self.screen.blit(self.background, (0,0))
		text = 'You lost :('
		if did_win:
			text = "You Win!!"
		self.game_over = pygame.font.Font('freesansbold.ttf', 30).render("Game Over: {}".format(text) , True, WHITE)
		self.screen.blit(self.game_over, (self.width/2 - 70, self.height / 2 ))
		pygame.display.flip()
		# wait for a bit then exit
		time.sleep(5)
		sys.exit(0)



	def opponent_attack(self, x, y):
		# use own board since opponent attacked
		tx, ty = self.coord_to_tile(x,y)
		tile_val = self.game_board[ty][tx]
		if tile_val not in ('HIT', 0, 'MISS'):
			left = tx * self.tilesize + self.xmargin + 40
			top = ty * self.tilesize + self.ymargin + 40

			self.game_board[ty][tx] = 'HIT'
			pygame.draw.rect(self.screen, BLACK, (self.width-250, self.height-200,149,23))
			self.opp_attack_id_surf = pygame.font.Font('freesansbold.ttf', 20).render("YOU WERE HIT", True, WHITE)
			self.screen.blit(self.opp_attack_id_surf, (self.width-250, self.height-200))
			self.lives -= 1
			if self.did_lose():
				self.end_game_screen(False)
		else:
			left = tx * self.tilesize + self.xmargin + 40
			top = ty * self.tilesize + self.ymargin + 40

			self.game_board[ty][tx] = 'MISS'
			pygame.draw.rect(self.screen, BLACK, (self.width-250, self.height-200,149,23))

			self.opp_attack_id_surf = pygame.font.Font('freesansbold.ttf', 20).render("YOU'RE SAFE", True, WHITE)
			self.screen.blit(self.opp_attack_id_surf, (self.width-250, self.height-200))
		self.turn += 1
		pygame.display.flip()
		return self.game_board[ty][tx]


	def attack(self, tile):
		# use opponents board since player is attacking
		tx, ty = tile
		if self.opponent_board[ty][tx] in ('MISS', 'HIT'):
			return False
		else:
			if self.opponent_board[ty][tx] != 0:
				left = tx * self.tilesize + self.xmargin + 40
				top = ty * self.tilesize + self.ymargin + 40
				pygame.draw.rect(self.screen, GREEN, (left, top, self.tilesize, self.tilesize))
				self.opponent_board[ty][tx] = 'HIT'
				if self.did_lose():
					self.end_game_screen(True)
			else:
				left = tx * self.tilesize + self.xmargin + 40
				top = ty * self.tilesize + self.ymargin + 40
				self.opponent_board[ty][tx] = 'MISS'
				pygame.draw.rect(self.screen, RED, (left, top, self.tilesize, self.tilesize))
		pygame.display.flip()
		self.turn += 1
		return True

	def main(self):
		# main event loop called that checks to see if any events (i.e. mouse clicks) have occured
		for event in pygame.event.get():
			if not self.is_turn():
				continue
			if event.type == MOUSEBUTTONDOWN:
				mousex, mousey = event.pos
				tile = self.coord_to_tile(mousex, mousey)
				if not tile:
					# mouse click was outside the board and thus invalid
					continue
				if not self.attack(tile):
					continue
				queue.put("x:{} y:{}".format(mousex, mousey))

		if self.is_turn():
			pygame.draw.rect(self.screen, BLACK, (self.width-250, self.height-340,193,23))
			self.turn_id_surf = pygame.font.Font('freesansbold.ttf', 20).render("YOUR TURN", True, WHITE)
			self.screen.blit(self.turn_id_surf, (self.width-250, self.height-340))
		else:
			pygame.draw.rect(self.screen, BLACK, (self.width-250, self.height-340,193,23))
			self.turn_id_surf = pygame.font.Font('freesansbold.ttf', 20).render("OPPONENTS TURN", True, WHITE)
			self.screen.blit(self.turn_id_surf, (self.width-250, self.height-340))

		pygame.display.flip()



