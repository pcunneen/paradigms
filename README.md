Partners: Pierce Cunneen and Alex Mukasyan

Setup:
  To start our version of battleship, in one terminal window type `python start.py 1`. Then in a second window, type `python start.py 2`. This will start up the game. The ships for each team are placed randomly. You will not know the position of your own ships, leaving you with more suspense as the game moves onwards.

Details:
  When you tap a square, if you hit an opponent, the square will light up green. If you miss the square will light up red. The game ends when one side has sunk all of the other ships. The game informs you when it is your turn to go and will also tell you the result of your opponent's move.